#ifndef JET_LINKED_FLOW_WRITER_H
#define JET_LINKED_FLOW_WRITER_H

#include "IJetLinkWriter.h"
#include "JetLinkWriterConfig.h"

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"
#include "xAODBase/IParticleContainer.h"

namespace H5 {
  class Group;
}

class JetLinkedFlowWriter
{
public:
  JetLinkedFlowWriter(H5::Group& output_group,
                       const JetLinkWriterConfig& config);

  ~JetLinkedFlowWriter();
  JetLinkedFlowWriter(JetLinkedFlowWriter&) = delete;
  JetLinkedFlowWriter operator=(JetLinkedFlowWriter&) = delete;
  JetLinkedFlowWriter(JetLinkedFlowWriter&&);
  void write(const xAOD::Jet& jet);
  void flush();
private:
  std::unique_ptr<IJetLinkWriter> m_writer;
};

#endif
