#ifndef JET_ELECTRON_WRITER_H
#define JET_ELECTRON_WRITER_H

#include "IJetLinkWriter.h"
#include "JetLinkWriterConfig.h"

#include "xAODJet/JetFwd.h"

// Standard Library things
#include <string>
#include <vector>
#include <memory>

namespace H5 {
  class Group;
}

class JetElectronWriter
{
public:
  JetElectronWriter(
    H5::Group& output_file,
    const JetLinkWriterConfig& config);
  ~JetElectronWriter();
  JetElectronWriter(JetElectronWriter&) = delete;
  JetElectronWriter operator=(JetElectronWriter&) = delete;
  JetElectronWriter(JetElectronWriter&&);
  void write(const xAOD::Jet& jet);
  void flush();
private:
  std::unique_ptr<IJetLinkWriter> m_writer;
};

#endif
