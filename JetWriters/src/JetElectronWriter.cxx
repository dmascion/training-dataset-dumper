#include "JetWriters/JetElectronWriter.h"
#include "JetWriters/JetElement.h"
#include "JetWriters/AssociationConfig.h"
#include "JetWriters/JetLinkWriter.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODBase/IParticle.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/Jet.h"

namespace {

  namespace jw = jetwriters;

  // define functions to retrieve cluster and track objects from electron
  auto getAssociatedElectronTrackConfig() {

    using Type = xAOD::Electron;
    using ElementType = JetElement<Type>;
    using PartLinks = std::vector<ElementLink<xAOD::IParticleContainer>>;

    // cluster getter
    auto cluster = [](const ElementType& el) -> const xAOD::CaloCluster* {
      if (!el.constituent) throw std::runtime_error("missing object");
      const auto* obj = el.constituent->caloCluster();
      if (!obj) throw std::runtime_error("neutral object missing");
      const auto* cluster = dynamic_cast<const xAOD::CaloCluster*>(obj);
      if (!cluster) throw std::runtime_error("can't cast to calo cluster");
      return cluster;
    };

    // track getter
    auto track = [](const ElementType& el) -> const xAOD::TrackParticle* {
      if (!el.constituent) throw std::runtime_error("missing object");
      const auto* obj = el.constituent->trackParticle();
      if (!obj) throw std::runtime_error("charged object missing");
      const auto* track = dynamic_cast<const xAOD::TrackParticle*>(obj);
      if (!track) throw std::runtime_error("can't cast to track particle");
      return track;
    };

    // build the association tuple
    return std::tuple{
      jw::getAssociationConfig(
        [cluster] (const ElementType& e) -> const xAOD::CaloCluster* {
          return cluster(e);
        }, "cluster"),
      jw::getAssociationConfig(
        [track](const ElementType& e) -> const xAOD::TrackParticle* {
          return track(e);
        }, "track")
    };
  }
}

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
JetElectronWriter::JetElectronWriter(
  H5::Group& output_file,
  const JetLinkWriterConfig& cfg):
  m_writer(nullptr)
{
  if (cfg.constituents.size != 0) {
    using T = xAOD::Electron;
    using I = xAOD::IParticleContainer;
    using A = decltype(getAssociatedElectronTrackConfig());
    m_writer.reset(
      new JetLinkWriter<T,I,A>(
        output_file, cfg, getAssociatedElectronTrackConfig()));
  }
}

JetElectronWriter::~JetElectronWriter() {
  flush();
}

JetElectronWriter::JetElectronWriter(JetElectronWriter&&) = default;

void JetElectronWriter::write(const xAOD::Jet& jet){
  if (m_writer) m_writer->write(jet);
}

void JetElectronWriter::flush() {
  if (m_writer) m_writer->flush();
}
