#include "HitWriter.hh"
#include "HitDecorator.hh"
#include "HitWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

#include "xAODTracking/Vertex.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

typedef std::function<float(const HitOutputs&)> FloatFiller;
class HitConsumers: public H5Utils::Consumers<const HitOutputs&> {};
class HitOutputWriter: public H5Utils::Writer<1,const HitOutputs&>
{
public:
  HitOutputWriter(H5::Group& file,
                  const std::string& name,
                  const HitConsumers& cons,
                  size_t size):
    H5Utils::Writer<1,const HitOutputs&>(file, name, cons, {{size}}) {}
};

Acc<char> isSct("isSCT");

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
HitWriter::HitWriter(
  H5::Group& output_file,
  const HitWriterConfig& config):
  m_hdf5_hit_writer(nullptr),
  m_dR_to_jet(config.dR_to_jet),
  m_save_endcap_hits(config.save_endcap_hits),
  m_save_only_clean_hits(config.save_only_clean_hits),
  m_bec(Acc<int>("bec"))
{

  HitConsumers fillers;
  add_hit_variables(fillers);

  // build the output dataset
  if (config.name.size() == 0) {
    throw std::logic_error("hit output name not specified");
  }
  if (config.output_size == 0) {
    throw std::logic_error("can't make an output writer with no hits");
  }
  
  m_hdf5_hit_writer.reset(
    new HitOutputWriter(
      output_file, config.name, fillers, config.output_size));
}

HitWriter::~HitWriter() {
  if (m_hdf5_hit_writer) m_hdf5_hit_writer->flush();
}

HitWriter::HitWriter(HitWriter&&) = default;

void HitWriter::sortHitsByDR(std::vector<const xAOD::TrackMeasurementValidation*>& hits,
                             const xAOD::Jet& jet,
                             const xAOD::Vertex& vx) { 
  auto sortRuleDR = [jet,vx] (const xAOD::TrackMeasurementValidation* h1, 
  		              const xAOD::TrackMeasurementValidation* h2) -> bool
  {
    TVector3 h1Pos(h1->globalX() - vx.x(), h1->globalY() - vx.y(), h1->globalZ() - vx.z());
    TVector3 h2Pos(h2->globalX() - vx.x(), h2->globalY() - vx.y(), h2->globalZ() - vx.z());   
    return jet.p4().Vect().DeltaR(h1Pos) < jet.p4().Vect().DeltaR(h2Pos);
  };
  std::sort(hits.begin(), hits.end(), sortRuleDR);
}

void HitWriter::write(const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
                      const xAOD::Jet& jet,
		      const xAOD::Vertex& vx) {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    
    // sort hits in decreasing dR from jet axis
    auto sortedHits = hits;
    sortHitsByDR(sortedHits, jet, vx);

    for (const auto hit: sortedHits) {
      // Calculate dR of hit to jet
      TVector3 hitPos(hit->globalX() - vx.x(), hit->globalY() - vx.y(), hit->globalZ() - vx.z());
      bool withinDR = (jet.p4().Vect().DeltaR(hitPos) < m_dR_to_jet);
      
      // The hits are already sorted so if we found a hit that is not within the dR
      // radius we want to save we can skip going through the rest of the hits
      if (!withinDR) break;
      
      // Check if the hit should be saved (e.g. EC vs barrel)
      bool isBarrel = (m_bec(*hit) == 0);
      bool saveHit = isBarrel || (!isBarrel && m_save_endcap_hits);
            
      // Only save hits that we want to save
      if (withinDR && saveHit) {
        hit_outputs.push_back(HitOutputs{hit, hitPos, &jet, &vx});
      }
    }
    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::write_dummy() {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::add_hit_variables(HitConsumers& vars) {
  
  // valid flag, true for any hit that is defined
  vars.add("valid", [](const auto&) { return true; }, false);

  // local and global hit positions
  vars.add("x_local", [](const HitOutputs& h){ return h.hit->globalX() - h.pv->x(); }, NAN);
  vars.add("y_local", [](const HitOutputs& h){ return h.hit->globalY() - h.pv->y(); }, NAN);
  vars.add("z_local", [](const HitOutputs& h){ return h.hit->globalZ() - h.pv->z(); }, NAN);
  vars.add("x_global", [](const HitOutputs& h){ return h.hit->globalX(); }, NAN);
  vars.add("y_global", [](const HitOutputs& h){ return h.hit->globalY(); }, NAN);
  vars.add("z_global", [](const HitOutputs& h){ return h.hit->globalZ(); }, NAN);
  vars.add("deta", [](const HitOutputs& h){ return h.jet->p4().Eta()-h.hitPos.Eta(); }, NAN);
  vars.add("dphi", [](const HitOutputs& h){ return h.jet->p4().Vect().DeltaPhi(h.hitPos); }, NAN);
  vars.add("abs_deta", [](const HitOutputs& h){ return std::copysign(1.0, h.jet->p4().Eta())*(h.hitPos.Eta() - h.jet->p4().Eta()); }, NAN);
  
  vars.add("radius", [](const HitOutputs& h){ 
           TVector3 hitPos(h.hit->globalX(), h.hit->globalY(), h.hit->globalZ());  
	   return hitPos.Perp(); }, NAN);
  
  // Hit layer (integer 0-4) and isBarrel (bec=0: barrel, bec=+-2: endcap)
  vars.add("layer", [acc=Acc<int>("layer")](const HitOutputs& h){ return acc(*h.hit); }, -1);
  vars.add("isBarrel", [acc=Acc<int>("bec")](const HitOutputs& h){ return (acc(*h.hit) == 0); }, false);  

  //this variable is saved as 1 if the hit come from the SCT, 0 if the hit come from the pixel, -1 elsewhere.
  vars.add<char> ("isSCT", [](const HitOutputs& h){ return isSct(*h.hit); }, -1);
  //The following variables are settet as -1 when the hit comes from the SCT subdetector, because that hits don't have these information into the AOD.
  // Information about hit quality
  vars.add<char> ("isFake", [acc=Acc<char>("isFake")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  vars.add<char> ("broken", [acc=Acc<char>("broken")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  vars.add("DCSState", [acc=Acc<int>("DCSState")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  vars.add("hasBSError", [acc=Acc<int>("hasBSError")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  
  // Information about split hits
  vars.add("isSplit", [acc=Acc<int>("isSplit")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  vars.add("splitProbability1", [acc=Acc<float>("splitProbability1")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  vars.add("splitProbability2", [acc=Acc<float>("splitProbability2")](const HitOutputs& h){ return isSct(*h.hit) ? -1 :  acc(*h.hit); }, -1);
  
}
