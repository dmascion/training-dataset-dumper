#include "TrackTruthDecorator.hh"

#include "TruthTools.hh"

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

// the constructor just builds the decorator
TrackTruthDecorator::TrackTruthDecorator(const std::string& prefix):
  m_track_truth_barcode(prefix + "truthBarcode"),
  m_track_parent_barcode(prefix + "truthParentBarcode"),
  m_truthLink("truthParticleLink"),
  m_truthLabel("truthOriginLabel")
{
}

const xAOD::TruthParticle* TrackTruthDecorator::getTruth( const xAOD::TrackParticle* track ) const {

  // retrieve the link and check its validity
  const TruthLink &link = m_truthLink(*track);

  // a missing or invalid link implies truth particle has been dropped from
  // the truth record at some stage - probably it was from pilup which by
  // default we don't store truth information for
  if(!link or !link.isValid()) {
    return nullptr;
  }

  return *link;
}


// this call actually does the work on the track
void TrackTruthDecorator::decorateAll(TrackSelector::Tracks tracks) const {

  for ( const auto& track : tracks ) {

    auto truth = getTruth(track);

    // store the truth barcode
    m_track_truth_barcode(*track) = truth ? truth->barcode() : -2;

    if(m_truthLabel.isAvailable(*track)){
      int parent_barcode = -2;

      int trackTruthLabel = m_truthLabel(*track);
      if ( trackTruthLabel == InDet::ExclusiveOrigin::FromB or
	   trackTruthLabel == InDet::ExclusiveOrigin::FromBC or
	   trackTruthLabel == InDet::ExclusiveOrigin::FromC) {
	auto parent_hadron = truth::getParentHadron(truth);
	if ( parent_hadron ) {
	  parent_barcode = parent_hadron->barcode();
	}
      }

      m_track_parent_barcode(*track) = parent_barcode;
    }

  }

}
