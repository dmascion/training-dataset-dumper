# https://its.cern.ch/jira/browse/ATR-26299
# ttbar files
mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.merge.AOD.e8453_e8455_s3873_s3874_r13983_r13831
# flat Z' files
mc21_13p6TeV.801271.Py8EG_A14NNPDF23LO_flatpT_Zprime.merge.AOD.e8464_e8455_s3873_s3874_r13983_r13831
