# Samples from tuning campaign for start of Run 3, see
#
# https://its.cern.ch/jira/browse/ATLMCPROD-9424
#
mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_e5984_s3126_d1677_r12711
mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.recon.AOD.e5362_e5984_s3126_d1677_r12711
